###############################################
##chatskill config for ThaiKoreEX SVN :: chatskill plugin
###############################################
#chatSkill_0 ���,���,���,��� �Ӿٴ�������ͷ��ʡ�źѾ��� ��������¤Ӿٴ ���ͧ�蹴�������ͧ���� , 
#chatSkill_0_type 1 �ٻẺ��ä��ҤӾٴ� chatSkill_x 0 = �Ӿٴ�е�ͧ����͹����������ҹ�� 
# 1 = �ӷ��������͹��ǹ���ǹ˹�觢ͧ����¤����
#chatSkill_0_emotion 26 ��ҹ emotion ᷹�Ӿٴ
#chatSkill_0_useSkill Heal ʡ�ŷ������ͷ��Ѻ�����ٴ
#chatSkill_0_lv 10 ����Ţͧʡ�ŷ�����
#chatSkill_0_msgType 15 �ٻẺ����Ѻ��ͤ��� 1 = �ٴ����� 2 = �ٴ㹻����� 4= �ٴ㹡�� 8 = ��ЫԺ 
# ��Ҩ��������Ẻ���ӵ���Ţ�Һǡ�ѹ�� ������ ���� ��� 2+4 = 6 ������ 15 ����Ѻ�ҡ��þٴ�ءẺ(1+2+4+8)
#chatSkill_0_target �кت���������� ��������� ������Ѻ�ء�� ����͹�����
#chatSkill_0_targetType 7 �ٻẺ������� 0=����ͧ 1= ������价�����������㹵�����͡�� 2 = ��㹵�� 4 = ��㹡�� 
# ��Ҩ���Ѻ����Ẻ�����Ţ�Һǡ�ѹ �� 㹵�� ���� ��� 2+4 = 6 ������ 7 ������Ѻ�ء��(1+2+4)
#chatSkill_0_minCastTime 0 ���ҷ��������Ƿ�����ش 
#chatSkill_0_maxCastTime 10 ���ҷ��������Ƿ�ҡ�ش
#chatSkill_0_waitAfterUse 2 ������ҧ�ͧ���ҷ�����ʡ�ŵ���
#chatSkill_0_playerTimeout 10 ��˹�����(�Թҷ�) �����ͨТ�ʡ�Ź�����ա������������Ҽ�ҹ����ǵ����˹�
###############################################

chatSkill_0 ���,���,���,���,Heal,heal,�ú�ش
chatSkill_0_type 1
chatSkill_0_emotion 26
chatSkill_0_useSkill Heal
chatSkill_0_lv 10
chatSkill_0_msgType 13
chatSkill_0_target 
chatSkill_0_targetType 7
chatSkill_0_minCastTime 0
chatSkill_0_maxCastTime 10
chatSkill_0_waitAfterUse 2
chatSkill_0_playerTimeout 15

chatSkill_1 ���,���,���,���,Heal,heal,�ú�ش
chatSkill_1_type 1
chatSkill_1_emotion 26
chatSkill_1_useSkill Heal
chatSkill_1_lv 10
chatSkill_1_msgType 12
chatSkill_1_target 
chatSkill_1_targetType 7
chatSkill_1_minCastTime 0
chatSkill_1_maxCastTime 10
chatSkill_1_waitAfterUse 2
chatSkill_1_playerTimeout 15

chatSkill_2 �ͺ�,�շ�,��,ab,�ú�ش,�Ѿ,�Ѻ,�ѿ,buff,Buff,AB
chatSkill_2_type 1
chatSkill_2_emotion 26
chatSkill_2_useSkill Blessing
chatSkill_2_lv 10
chatSkill_2_msgType 13
chatSkill_2_target 
chatSkill_2_targetType 7
chatSkill_2_minCastTime 0
chatSkill_2_maxCastTime 10
chatSkill_2_waitAfterUse 3
chatSkill_2_playerTimeout 60

chatSkill_3 a,agi,��
chatSkill_3_type 1
chatSkill_3_emotion 26
chatSkill_3_useSkill Increase AGI
chatSkill_3_lv 10
chatSkill_3_msgType 1
chatSkill_3_target 
chatSkill_3_targetType 7
chatSkill_3_minCastTime 0
chatSkill_3_maxCastTime 10
chatSkill_3_waitAfterUse 3.2
chatSkill_3_playerTimeout 60

chatSkill_4 im,���,�ú�ش,�Ѿ,�Ѻ,�ѿ,buff,Buff
chatSkill_4_type 1
chatSkill_4_emotion 26
chatSkill_4_useSkill Impositio Manus
chatSkill_4_lv 5
chatSkill_4_msgType 13
chatSkill_4_target 
chatSkill_4_targetType 7
chatSkill_4_minCastTime 0
chatSkill_4_maxCastTime 10
chatSkill_4_waitAfterUse 4
chatSkill_4_playerTimeout 30

chatSkill_5 षդѺ, k,abk,�ú�ش,�Ѿ,�Ѻ,�ѿ,buff,Buff,� ,�ͺ��
chatSkill_5_type 1
chatSkill_5_emotion 26
chatSkill_5_useSkill Kyrie Eleison
chatSkill_5_lv 10
chatSkill_5_msgType 13
chatSkill_5_target 
chatSkill_5_targetType 7
chatSkill_5_minCastTime 0
chatSkill_5_maxCastTime 10
chatSkill_5_waitAfterUse 4
chatSkill_5_playerTimeout 30