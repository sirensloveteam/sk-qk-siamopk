support_guild ��Archf!end��

SanctuaryAuto 0
SanctuaryAuto_posX 42
SanctuaryAuto_posY 242
SanctuaryAuto_autoStop 1


enableCoat 0
enableCoat_msg cccc
enableCoat_resp_0 no %$arg0 go to hell
enableCoat_resp_1 %$arg0 no f*ck you1
enableCoat_resp_2 %$arg0 no f*ck you2
enableCoat_resp_3 %$arg0 no f*ck you3
enableCoat_resp_4 %$arg0 no f*ck you4
enableCoat_resp_5 %$arg0 no f*ck you5

enableRepair 0
enableRepair_msg rrrrr
enableRepair_cmpltMsg go man %$arg0 ^^
enableRepair_failedMsg %$arg0 no f*ck you -*-
enableRepair_insufficientSP wait %$arg0


warpAuto 0
warpAuto_0_memberAround 1
warpAuto_0_posX 107
warpAuto_0_posY 283

warpAuto_1_memberAround 8
warpAuto_1_posX 107
warpAuto_1_posY 285

warpAuto_2_memberAround 14
warpAuto_2_posX 107
warpAuto_2_posY 287

warpAuto_autoStop 1

warpAuto_0 alde
warpAuto_0_map alde_gld

warpAuto_1 gef
warpAuto_1_map gef_fild13

warpAuto_2 pron
warpAuto_2_map prt_gld

warpAuto_3 pay
warpAuto_3_map pay_gld

warpAuto_msg_0 %$arg0 go away to my warp.

guildSkill_enable 0
guildSkill_distance 9
guildSkill_checkWall 1

guildSkill Blessing {
	lvl 10
	maxCastTime 1
	minCastTime 0
	targetInStatus 0
	targetOutStatus Blessing
	targetJob 
	targetTimeout 5
}

guildSkill Increase AGI {
	lvl 10
	maxCastTime 2
	minCastTime 0
	targetInStatus 0
	targetOutStatus Increase AGI
	targetJob 
	targetTimeout 5
}

guildSkill Assumptio {
	lvl 5
	maxCastTime 4
	minCastTime 0
	targetInStatus 0
	targetOutStatus Assumptio
	targetJob 
	targetTimeout 5
}

guildSkill Suffragium {
	lvl 3
	maxCastTime 1
	minCastTime 0
	targetInStatus 0
	targetOutStatus Suffragium
	targetJob Wizard,High Wizard
	targetTimeout 5
}

guildSkill Impositio Manus {
	lvl 5
	maxCastTime 1
	minCastTime 0
	targetInStatus 0
	targetOutStatus Impositio Manus
	targetJob Knight,Blacksmith,Hunter,Assassin,Peco Knight,Crusader,Monk,Rogue,Peco Crusader,Gunslinger,Ninja,Lord Knight,Whitesmith,Sniper,Assassin Cross,Peco Lord Knight,Paladin,Champion,Peco Paladin
	targetTimeout 5
}

guildSkill Blessing {
	lvl 10
	maxCastTime 1
	minCastTime 0
	targetInStatus Cursed
	targetOutStatus 0
	targetJob 
	targetTimeout 5
}

guildSkill Lex Divina {
	lvl 5
	maxCastTime 1
	minCastTime 0
	targetInStatus Silenced
	targetOutStatus 0
	targetJob 
	targetTimeout 5
}

guildSkill Status Recovery {
	lvl 1
	maxCastTime 1
	minCastTime 0
	targetInStatus Petrified,Frozen,Stunned,Petrifying
	targetOutStatus 0
	targetJob 
	targetTimeout 5
}

#Neuschwanstein
memo_0 alde1
memo_0_map alde_gld
memo_0_posX 49
memo_0_posY 84

#Hohenschwangau
memo_1 alde7
memo_1_map alde_gld
memo_1_posX 96
memo_1_posY 247

#Nuernberg
memo_2 alde2
memo_2_map alde_gld
memo_2_posX 142
memo_2_posY 87

#Wurzburg
memo_3 alde9
memo_3_map alde_gld
memo_3_posX 234
memo_3_posY 243

#Rothenburg
memo_4 alde3
memo_4_map alde_gld
memo_4_posX 270
memo_4_posY 90

#Leprion
memo_5 gef1
memo_5_map gef_fild13
memo_5_posX 155
memo_5_posY 48

#Jyolbriger
memo_6 gef9
memo_6_map gef_fild13
memo_6_posX 306
memo_6_posY 237

#Yssnelf
memo_7 gef7
memo_7_map gef_fild13
memo_7_posX 77
memo_7_posY 295

#Bergel
memo_8 gef8
memo_8_map gef_fild13
memo_8_posX 190
memo_8_posY 275

#Melcedez
memo_9 gef3
memo_9_map gef_fild13
memo_9_posX 248
memo_9_posY 56

#Kriemhild
memo_10 pron1
memo_10_map prt_gld
memo_10_posX 142
memo_10_posY 64

#Swanhild
memo_11 pron3
memo_11_map prt_gld
memo_11_posX 240
memo_11_posY 131

#Lazreagues
memo_12 pron5
memo_12_map prt_gld
memo_12_posX 153
memo_12_posY 133

#Skoegul
memo_13 pron7
memo_13_map prt_gld
memo_13_posX 126
memo_13_posY 239

#Kendul
memo_14 pron9
memo_14_map prt_gld
memo_14_posX 193
memo_14_posY 240

#Arbor of Light
memo_15 pay7
memo_15_map pay_gld
memo_15_posX 122
memo_15_posY 230

#Garden of Heaven
memo_16 pay3
memo_16_map pay_gld
memo_16_posX 299
memo_16_posY 116

#Shadow of Buddha
memo_17 pay9
memo_17_map pay_gld
memo_17_posX 316
memo_17_posY 293

#Red Palace
memo_18 pay1
memo_18_map pay_gld
memo_18_posX 140
memo_18_posY 163

#Bamboo Forest Hermitage
memo_19 pay5
memo_19_map pay_gld
memo_19_posX 193
memo_19_posY 267