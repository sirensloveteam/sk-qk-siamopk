#[item]
#key=value
#-------------------[format]-------------------
#item	:item name from list of tables\items.txt
#key	:list below
#value	:0 is off , 1 is turn on
#----------------------[key]-----------------------
#invKeep			:max capacity of inventory  {0...999}
#invMinAmount		:min capacity for back to get it {0...999}
#invPassive		:if turn on , invMinAmount is not important {0|1}
#invToStorage		:if turn on , this item will store in storage first {0|1}
#invToCart		:if turn on , this item will store in cart in number of cartKeep {0|1}
#cartKeep			:max capacity in cart {0..999}
#cartMinAmount		:min capacity in cart for back to get it {0...999}
#cartPassive		:if turn on , cartMinAmount is not important {0|1}
#cartToStorage		:if turn on , item in cart will store in storage {0|1}
#cartToInv			:if turn on , item in cart will store in inventroy by number of invKeep {0|1}
#storageToInv		:if turn on , item in storage will store in inventroy by number of invKeep {0|1}
#storageToCart		:if turn on , item in storage will store in cart by number of cartKeep {0|1}
#sell			:sell flage {0|1}
#deal			:if turn on , this item will send to other that set in config.txt  by function dealautostroage {0|1}
#buyToInv		:The number of  NPC that set in  Config.txt  by  invKeep {0 for not buy, 1 2 3 4 for buy #npc number} 
#------------------------------------------------------------------------------------------
# 'all' is set for give all to storage
[all]
invKeep=0
invMinAmount=0
invPassive=false
invToStorage=true
invToCart=false
cartKeep=0
cartMinAmount=0
cartPassive=false
cartToStorage=true
cartToInv=false
storageToInv=false
storageToCart=false
sell=false
deal=false
buyToInv=false


#Special Item no sell, no storage, keep on inventory all time
[Wedding Ring,The Sign,Novice Adventurer's Suit[1],Novice Fly Wing,Novice Butterfly Wing,Mark Of Paradise Team,Medic's Robe[1],Valor Badge,Bravery Badge,Paradise Team Uniform3,Paradise Team Boots3]
invToStorage=no

