doCommand autobuy {
    hp 0<100
    sp 0<100
    spirits 0<0
    inLockOnly 0
    maxAggressives 0
    minAggressives 0        
    stopWhenHit 0
    stopWhenSteal 0
    stopWhenTotalDmg 0
    monsters
    monRace
    monInStatus 0
    monOutStatus 0
    timeout 1
    inStatus 0
    outStatus 0
    whileSitting 0
    notWhileSitting 0
    waitAfterKill 0
    whenAI
    notWhenAI
    prevSkill
    inMap need wing or skill
    inMsg 
}


doCommand {
    hp 0<100
    sp 0<100
    spirits 0<0
    inLockOnly 0
    maxAggressives 0
    minAggressives 0        
    stopWhenHit 0
    stopWhenSteal 0
    stopWhenTotalDmg 0
    monsters
    monRace
    monInStatus 0
    monOutStatus 0
    timeout 60
    inStatus 0
    outStatus 0
    whileSitting 0
    notWhileSitting 0
    waitAfterKill 0
    whenAI
    notWhenAI
    prevSkill
    inMap 
    inMsg 
}