#----------------------[format]---------------------
#[monster's name]
#key=value


#attack			:attack when in sign, need to set  attackAuto in config.txt
#teleportFlag		:teleport setting
#			:1 = tele when in sign.
#			 2 = tele when any attack from this monster but ai is not Important Item
#		 	:3 = tele when any attack from this monster but Skalax is battle with this monster and ai is not Important Item
#		 	:4 = tele when distance is below teleportAuto_dist (plus_control)
#		 	:5 = look at plus_control
#			       teleportAuto_minAgNotorious
#			       teleportAuto_minAgWithAgNM
#teleportSearch		:  teleportAuto_search in plus_control.txt 
#teleportAgressive		:  number of this monster mob to tele
#skillcouterAttack		:  counter when cast skill
#aggressiveLvAttak		: sequence of monster important 1 2 3 4 5 6 7 8 9 if not use set  0 
#teleportAvoidDamage	:  when this monster attack dmg is over some tele if not use set 0
#counterAttack		: counter when attack need set in config by function  AutoCouterAgMonster
#------------------------------------------------------------------------------------------


