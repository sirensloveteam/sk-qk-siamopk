#################################################
#ThaiKoreEx :: doCommand plugin
#This plugin is licensed under the GNU GPL
#Original by kaliwanagan - http://www.openkore.com
#MOD by Valkyrie - http://www.thaikore.com
#Mod for ThaiKoreSVN by UltimaWeapon 17/12/2007
#Edit for ThaiKoreEx2.5 ONLY by Hunter Heal 17-June-2008
#################################################
package doCommand;

use strict;
use Time::HiRes qw/time/;
use Plugins;
use Globals;
use FileParser;
use miscFunctions ;
use Command;
use Utils;
our %docmd;
Plugins::register('doCommand', 'do a command on certain conditions', \&Unload);

Plugins::addHook(HOOK_START, \&start);
Plugins::addHook(HOOK_AI_POST, \&eventAI_post);

sub Unload {
	Plugins::delHook(HOOK_START, \&start);
	Plugins::delHook(HOOK_AI_POST, \&eventAI_post);
}

my $time;

sub start {
  addParseFiles(\@parseFiles, modifingPath('config', "docommand.txt"), \%docmd, \&parseDataFile2);
}

sub eventAI_post {
	my $prefix = "doCommand_";
	my $i = 0;

	while (1) {
		last if (!$docmd{$prefix.$i});

		if	(checkCondition(\%docmd, $prefix.$i, undef, $i) &&
			(!$docmd{$prefix.$i."_inMap"} || existsInList($docmd{$prefix.$i."_inMap"}, $field{name})) && 
			timeOut($time, $docmd{$prefix.$i."_timeout"})) {
			$command->parse($docmd{$prefix.$i});
			$time = time;
		}
		$i++;
	}
}

return 1;