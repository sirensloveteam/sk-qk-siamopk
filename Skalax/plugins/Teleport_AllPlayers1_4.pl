################################################
##ThaiKoreEx :: Teleport AllPlayers v.1.4 Plugins
## Teleport all player out of you party 
##This plugin is licensed under the GNU GPL
##For ThaiKoreEx 2.5  23-Sep-2008 - 24-Sep-2008
##Original from OpenKoreSVN
##mod : Hunter Heal  www.thaikore.com
## 1.Copy line below  to plus_control.txt
##   teleportAuto_Allplayers <value>
## <value> -> 0=off, 1 = telport all players on ,2 = save palyer name and teleport ,
#### 3 = teleport players in black list only , 4 = Record player [Date/time,ID,Name] (not teleport) save to \log\REC_Player.txt
##   relogTime_notTeleport 10
## 2.Copy line below  to timeouts.txt
##  ai_Teleport_allPlayers 1 
## 3. Copy blacklist.txt file to you_control folder
################################################

package Teleport_AllPlayers;

use strict;
no strict 'refs';
use Time::HiRes qw(time);
use Globals;
use Plugins;
use Utils qw(timeOut getFormattedDate);
use miscFunctions qw(useTeleport relog);
use FileParser qw(modifingPath addParseFiles parseDataFile2 parseDataFile);

our %saveID;
our $rectime;
our %blacklist;

my %saveID;
my $rectime;

Plugins::register(' Teleport_AllPlayers', 'Auto Teleport All Players v.1.4', \&Unload);

Plugins::addHook(HOOK_START, \&start);
Plugins::addHook(HOOK_AI_MOVE, \&eventAI_move);

sub Unload {
	Plugins::delHook(HOOK_AI_MOVE, \&eventAI_move);
	Plugins::delHook(HOOK_START, \&start);
}
sub start {
	addParseFiles(\@parseFiles, modifingPath('config', "blacklist.txt"), \%blacklist, \&parseDataFile2);
}

sub eventAI_move {
	return if ( !scalar(@playersID) || $config{"lockMap_$lockMapIndex"} ne $field{name} || !defined($config{teleportAuto_Allplayers}) || !$config{teleportAuto_Allplayers} );
	return if ( $AI->action eq 'dead' || $AI->inQueue(qw[talkAuto storageAuto sellAuto buyAuto]) || !defined($timeout{ai_Teleport_allPlayers}) );	
		
				foreach (@playersID) {
						next unless ($_);
						next if ( (ref($char->{party})  && $char->{party}{users}{$_}) );				
					if ( $config{teleportAuto_Allplayers} == 1 && timeOut($timeout{ai_Teleport_allPlayers}) ) {
						$interface->writeoutput("===> Teleporting to avoid all players <=== \n");									
						tele_now();
					}			
					if ( rec_player() && $config{teleportAuto_Allplayers} == 2  ) {
						$interface->writeoutput("===> Save player name and Teleporting <=== \n");		
							tele_now();
					}					
					if (find_blacklist() && $config{teleportAuto_Allplayers} == 3  ) {
						$interface->writeoutput("===> Teleporting player black list <=== \n");		
							tele_now();
					}
					if ( $config{teleportAuto_Allplayers} == 4  ) {	# Rec. [time , ID , name]
							rec_player()
					}													
			}				
}

##-- Utilities---##
sub rec_player {  ##-- Record  player--##
	my $ID = unpack("L1", $_) ;
	my $player = $actorList->get($_);
	my $date = getFormattedDate(int(time));
	my $dump = "#".$date."#".$ID."#".$player->{name}."#\n";
	if (!(exists $saveID{$ID}) ) {
		if ($player->{name} ne 'Unknow' && timeOut($rectime,0.5)) {
			$saveID{$ID}=$player->{name};
				#$interface->writeoutput("==Save=$ID==$player->{name}==$date== \n");		
					open DUMP, ">> ".modifingPath('log', "REC_Player.txt");
					print DUMP $dump;
					close DUMP;				
				$rectime=time;
			return 1;
		}
	}
return 0;
}
sub tele_now { ## Use Teleport and relog
	$ai_v{clear_aiQueue} = 1;					
		if ( !(useTeleport(1))  && ( $config{relogTime_notTeleport} ne '' || $config{relogTime_notTeleport} != 0 ) ) {
				$interface->writeoutput("===>Relog Can't teleport <=== \n");		
				relog( $config{relogTime_notTeleport} );
		}
	$timeout{ai_Teleport_allPlayers}{time} = time;
}

sub find_blacklist { ## Search Black List ID and value = 1
	my $ID = unpack("L1", $_) ;
	if ( (exists $blacklist{$ID}) && $blacklist{$ID} ) {
		$interface->writeoutput("== ID >> $ID ");		
		return 1;
	}
return 0;
}
return 1;