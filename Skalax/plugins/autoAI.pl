################################################
##ThaiKoreEx :: autoAI plugin
##This plugin is licensed under the GNU GPL
##Beta version 5 by Ninkoman
##For ThaiKoreEX 2.5  15/03/09
##Test on ThaikoreEX 2.5 Hunter Heal Mod. 11/08
################################################
package autoAI;

use strict;
use Globals;
use Plugins;
use FileParser;
use Utils;
use Command;
use miscFunctions;

our @autoAI;
our @autoAI_que;
our %autoAI_timeout;
our %autoAI_temp;
our $autoAIState = 1;
our $lastAuto;

Plugins::register('autoAI', 'autoAI for Thaikore EX 2.5', \&Unload);

Plugins::addHook(HOOK_START, \&start);
Plugins::addHook(HOOK_AI_PRE, \&ai_pre);
Plugins::addHook(HOOK_EVENT_CHAT, \&event_chat);
Plugins::addHook(HOOK_INPUT_PRE, \&input_pre);
Plugins::addHook(HOOK_EVENT_YOU, \&entering_game);
Plugins::addHook(HOOK_PACKET_POST, \&packet_post);
Plugins::addHook(HOOK_MSG_CONSOLE, \&msg_console);

sub Unload {	
	Plugins::delHook(HOOK_START, \&start);
	Plugins::delHook(HOOK_AI_PRE, \&ai_pre);
	Plugins::delHook(HOOK_EVENT_CHAT, \&event_chat);
	Plugins::delHook(HOOK_INPUT_PRE, \&input_pre);
	Plugins::delHook(HOOK_EVENT_YOU, \&entering_game);
	Plugins::delHook(HOOK_PACKET_POST, \&packet_post);
	Plugins::delHook(HOOK_MSG_CONSOLE, \&msg_console );
}

sub start {
	addParseFiles(\@parseFiles, modifingPath('config', "autoAI.tks"), \@autoAI, \&parseScript);
}

sub event_chat {
	return if(!$autoAIState);
	my $args = $_[1];
	my $chatMsg = $args->{message};
	my $type = $args->{type};
	for (my $i = 0; $autoAI[$i][0] ne "";$i++) {
		my @trigger = splitCommand($autoAI[$i][0], 2, 0);
		if (($type == EV_MESSAGE_CHAT && $trigger[0] eq "msgc" && existsInList($trigger[1],$chatMsg))
			|| ($type == EV_MESSAGE_CHAT && $trigger[0] eq "emsgc" && existsInPatternList($trigger[1], $chatMsg))
			|| ($type == EV_MESSAGE_CHAT && $trigger[0] eq "msgcr" && existsInList($trigger[1],$chatMsg) && $currentChatRoom ne '')
			|| ($type == EV_MESSAGE_CHAT && $trigger[0] eq "emsgcr" && existsInPatternList($trigger[1], $chatMsg) && $currentChatRoom ne '')
			|| ($type == EV_MESSAGE_PARTY && $trigger[0] eq "msgp" && existsInList($trigger[1],$chatMsg))
			|| ($type == EV_MESSAGE_PARTY && $trigger[0] eq "emsgp" && existsInPatternList($trigger[1], $chatMsg))
			|| ($type == EV_MESSAGE_GUILD && $trigger[0] eq "msgg" && existsInList($trigger[1],$chatMsg))
			|| ($type == EV_MESSAGE_GUILD && $trigger[0] eq "emsgg" && existsInPatternList($trigger[1], $chatMsg))
			|| ($type == EV_MESSAGE_PM && $trigger[0] eq "msgpm" && existsInList($trigger[1],$chatMsg) && $args->{name} ne $char->{name})
			|| ($type == EV_MESSAGE_PM && $trigger[0] eq "emsgpm" && existsInPatternList($trigger[1], $chatMsg) && $args->{name} ne $char->{name})
			|| ($type == EV_MESSAGE_NPC && $trigger[0] eq "msgnpc" && existsInList($trigger[1],$chatMsg))
			|| ($type == EV_MESSAGE_NPC && $trigger[0] eq "emsgnpc" && existsInPatternList($trigger[1], $chatMsg))
			|| ($type == EV_MESSAGE_GM && $trigger[0] eq "msggm" && existsInList($trigger[1],$chatMsg))
			|| ($type == EV_MESSAGE_GM && $trigger[0] eq "emsggm" && existsInPatternList($trigger[1], $chatMsg))) {
			insert_que($i);
		}
	}
}

sub ai_pre {
	if(timeOut($autoAI_timeout{delay2}) && defined($autoAI_timeout{delay2}{timeout})){
		$autoAIState = 1;
		undef $autoAI_timeout{delay2}{timeout};
	}
	return if(!defined($autoAI[0][0]) || $autoAIState != 1);
	my @autoAI_step_now = splitCommand($autoAI_que[0]);
	if ($autoAI_step_now[0] eq "command") {
		autoAI_step_command($autoAI_que[0]);
	}elsif ($autoAI_step_now[0] eq "move") {
		autoAI_step_move($autoAI_que[0]);
	}elsif ($autoAI_step_now[0] eq "delay") {
		autoAI_step_delay($autoAI_que[0]);
	}elsif ($autoAI_step_now[0] eq "delay2") {
		autoAI_step_delay2($autoAI_step_now[1]);
	}elsif ($autoAI_step_now[0] eq "autoAI") {
		my @autoAI_step_now2 = splitCommand($autoAI_que[0],2,0);
		autoAI_step_autoAI($autoAI_step_now2[1]);
	}elsif ($autoAI_step_now[0] eq "talk") {
		autoAI_step_talkNPC($autoAI_que[0]);
	}elsif ($autoAI_step_now[0] eq "setTimeStart") {
		autoAI_step_settime($autoAI_step_now[1]);
	}elsif ($autoAI_step_now[0] eq "setTimeDelete") {
		autoAI_step_settimedel($autoAI_step_now[1]);
	}elsif ($autoAI_step_now[0] eq "setTimeClear") {
		undef %{$autoAI_temp{settime}};
		$interface->writeoutput("autoAI: settime: all cleared.\n","yellow");
	}elsif ($autoAI_step_now[0] eq "direction") {
		autoAI_step_direction($autoAI_step_now[1],$autoAI_step_now[2]);
	}elsif ($autoAI_step_now[0] eq "saveLog") {
		my @autoAI_step_now2 = splitCommand($autoAI_que[0],2,0);
		autoAISaveLog($autoAI_step_now2[1]);
		shift @autoAI_que;
	}elsif ($autoAI_step_now[0] eq "changeScript") {
		changeScript($autoAI_step_now[1]);
		shift @autoAI_que;
	}

	if ($autoAI_step_now[0] ne "move") {
		#old is 0.5
		$autoAI_timeout{move_rep}{timeout} = 1;
	}

	for (my $i = 0; $autoAI[$i][0] ne "";$i++) {
		my @trigger = splitCommand($autoAI[$i][0], 2, 0);
		if ($trigger[0] eq "condition" && selfCondition($trigger[1]) && ($lastAuto != $i || !defined($autoAI_que[0]))) {
			$lastAuto = $i;
			insert_que($i);

		} elsif ($trigger[0] eq "check") {
			my @trigger2 = splitCommand($trigger[1], 2, 0);
			if ($trigger2[0] eq "item_upper") {
				my ($itemID,$itemVal,$force) = splitCommand($trigger2[1], 3, 0);
				for (my $k = 0; $k < 150; $k++) {
					next if($char->{inventory}[$k]{nameID} eq "");
					if ($char->{inventory}[$k]{nameID} == $itemID && $char->{inventory}[$k]{amount} > $itemVal && ($lastAuto != $i || !defined($autoAI_que[0]))) {
						$lastAuto = $i;
						insert_que($i);
					}
				}
			} elsif ($trigger2[0] eq "item_lowwer") {
				my ($itemID,$itemVal,$force) = splitCommand($trigger2[1], 3, 0);
				for (my $k = 0; $k < 150; $k++) {
					next if($char->{inventory}[$k]{nameID} eq "");
					if ($char->{inventory}[$k]{nameID} == $itemID) {
						if ($char->{inventory}[$k]{amount} >= $itemVal || $lastAuto == $i || !defined($autoAI_que[0])) {
							goto LASTCHECK;
						}else{
							$lastAuto = $i;
							insert_que($i);
						}
					}
				}
			LASTCHECK:
			}

		} elsif ($trigger[0] eq "timeIn") {
			my @trigger2 = splitCommand($trigger[1]);
			if (defined($autoAI_temp{settime}{"${trigger2[0]}"}) && !timeOut($autoAI_temp{settime}{"${trigger2[0]}"},$trigger2[1]) && ($lastAuto != $i || !defined($autoAI_que[0]))) {
				$lastAuto = $i;
				insert_que($i);
			}

		} elsif ($trigger[0] eq "timeOut") {
			my @trigger2 = splitCommand($trigger[1]);
			if (defined($autoAI_temp{settime}{$trigger2[0]}) && timeOut($autoAI_temp{settime}{$trigger2[0]},$trigger2[1]) && ($lastAuto != $i || !defined($autoAI_que[0]))) {
				$lastAuto = $i;
				insert_que($i);
			}

		} elsif ($trigger[0] eq "checkVar") {
			my @trigger2 = splitCommand($trigger[1]);
			my $count = scalar(@trigger2);
			for (my $i = 0; $i < $count;$i=$i+2) {
				if($autoAI_temp{varset}{$trigger2[$i]} ne $trigger2[$i+1]){
					goto ENDCHECKVAR;
				}
			}
			$lastAuto = $i;
			insert_que($i);
		ENDCHECKVAR:
		}
	}
	checkScriptTime(int(time)) if ($config{autoAI_0_start} ne '');

}

sub input_pre {
	return if(!defined($autoAI[0][0]));
	my $args = $_[1];
	my $input = $args->{input};
	my $switch = $args->{switch};
	if($switch eq "autoAI") {
		autoAI_step_autoAI($input,1);

	}elsif($switch eq "drop2"){
		my ($item, $amount) = $input =~ /(.+?)\s+((?:-)?\d+.*)/;
		my $invIndex = findIndexString_lc($char->{inventory}, 'name', $item);
		if (!defined($invIndex)) {
			$interface->writeoutput("autoAI: drop2 : $item not found.\n","yellow");
			return;
		}
		$amount = $char->{inventory}[$invIndex]{amount} if (!$amount || $amount > $char->{inventory}[$invIndex]{amount});
		$msgSender->sendDrop($char->{inventory}[$invIndex]{index}, $amount);

	}elsif($switch eq "is2"){
		my $invIndex = findIndexString_lc($char->{inventory}, 'name', $input);
		if (!defined($invIndex)) {
			$interface->writeoutput("autoAI: is2 : $input not found.\n","yellow");
			return;
		}
		$msgSender->sendItemUse($char->{inventory}[$invIndex]{index}, $accountID);

	}elsif($switch eq "relog2"){
		my @cmd = splitCommand($input, 3, 0);
		if ($cmd[0] < 24 && $cmd[0] ne '' && $cmd[1] < 60 && $cmd[1] ne '' && $cmd[2] < 60 && $cmd[2] ne '') {
			my @localtime = localtime int(time);
			my $relogtime = $cmd[0]*3600 + $cmd[1]*60 + $cmd[2];
			my $timenow = $localtime[2]*3600 + $localtime[1]*60 + $localtime[0];
			my $relogcount;
			if ($relogtime <= $timenow) {
				$relogcount = (86400 - $timenow) + $relogtime;
			}else{
				$relogcount = $relogtime - $timenow;
			}
			relog($relogcount);
			$interface->writeoutput("autoAI: relog2 : relog on ".$cmd[0].":".$cmd[1].":".$cmd[2]." .\n","yellow");
		}else{
			$interface->writeoutput("autoAI: relog2 : syntax error!\n","yellow");
			$interface->writeoutput("autoAI: relog2 : syntax: relog2 <0-23> <0-59> <0-59>\n","yellow");
		}

	}elsif($switch eq "saveLog"){
		autoAISaveLog($input);

	}elsif($switch eq "join2"){
		my ($replace, $titleList) = $input =~ /(^\"(.*?)\" ?)/;
		my $qm = quotemeta $replace;
		$input =~ s/$qm//;

		my $roomID = serchChatRoomIDByPatternTitle($titleList);
		if ($roomID ne '') {
			if ($currentChatRoom eq '') {
				$msgSender->sendChatRoomJoin($roomID,$input);
			}else{
				$interface->writeoutput("autoAI: join2 : You are already in a chat room!\n","yellow");
			}
		}else{
			$interface->writeoutput("autoAI: join2 : syntax error!\n","yellow");
			$interface->writeoutput("autoAI: join2 : syntax: join2 \"<pattern list>\" <password>\n","yellow");
		}
	}
}

sub entering_game {
	my $args = $_[1];
	return if($args->{type} != EV_ENTERING_GAME);
	for (my $i = 0; $autoAI[$i][0] ne "";$i++) {
		my @trigger = splitCommand($autoAI[$i][0], 2, 0);
		if ($trigger[0] eq "entering_game") {
			insert_que($i);
		}
	}
}

sub packet_post {
	my $args = $_[1];
	my $switch = $args->{switch};
	return if(!$autoAI_temp{talknpc}{talked} || !defined($autoAI[0][0]) || !$autoAIState);
	
	if ($switch eq "00B5" || $switch eq "00B6" || $switch eq "00B7" || $switch eq "0142" || $switch eq "01D4") {
		my @cmd = splitCommand($autoAI_que[0],4,0);
		return if($cmd[0] ne "talk");
		if ($cmd[3] eq "") {
			$msgSender->sendTalkCancel(pack("L", $autoAI_temp{talknpc}{ID}));
			$autoAI_temp{talknpc}{talked} = 0;
			shift @autoAI_que;
			return;
		}
		my @step = splitCommand($cmd[3]);
		my $stepkey = substr($step[0],0,1);
		my $stepval = substr($step[0],1,length($step[0] - 1));
		my $arg;
		# old is 0.5
		sleep(3);
		if ($stepkey eq "c") {
			$msgSender->sendTalkContinue(pack("L", $autoAI_temp{talknpc}{ID}));
			$autoAI_timeout{talk_done} = time;
			$interface->writeoutput("autoAI: npc talk : continue.\n","yellow");
		} elsif($stepkey eq "n") {
			$msgSender->sendTalkCancel(pack("L", $autoAI_temp{talknpc}{ID}));
			$autoAI_temp{talknpc}{talked} = 0;
			shift @autoAI_que;
			undef $autoAI_timeout{talk_done};
			$interface->writeoutput("autoAI: npc talk : done.\n","yellow");
			# old is 0.5
			sleep(3);
			return;
		} elsif($stepkey eq "r") {
			$msgSender->sendTalkResponse(pack("L", $autoAI_temp{talknpc}{ID}), $stepval + 1);
			$autoAI_timeout{talk_done} = time;
			$interface->writeoutput("autoAI: npc talk : respon ${stepval}.\n","yellow");
		} elsif($stepkey eq "a") {
			my $stepkey2 = substr($stepval,0,1);
			my $stepval2 = substr($stepval,1,length($stepval) - 1);
			if ($stepkey2 eq "d") {
				$msgSender->sendTalkAnswerNumber(pack("L", $autoAI_temp{talknpc}{ID}), $stepval2);
				$interface->writeoutput("autoAI: npc talk : digit ${stepval2}.\n","yellow");
			} elsif ($stepkey2 eq "t") {
				$msgSender->sendTalkAnswerText(pack("L", $autoAI_temp{talknpc}{ID}), $stepval2);
				$interface->writeoutput("autoAI: npc talk : text ${stepval2}.\n","yellow");
			}
			$autoAI_timeout{talk_done} = time;
		}else {
			$interface->writeoutput("autoAI: ERROR - talk command syntax error 2.\n","yellow");
		}
		my ($i,$newstep);
		for ($i = 1; $i < scalar(@step); $i++) {
			$newstep .= " $step[$i]";
		}
		$autoAI_que[0] = "$cmd[0] $cmd[1] $cmd[2]$newstep";
	}elsif($switch eq "00B4"){
		my ($ID, $talk) = unpack("x4 a4 Z*", $args->{msg});

		if (unpack("V", $ID)) {
			$talk =~ s/\^[0-9a-fA-F]{6}//g;
			for (my $i = 0; $autoAI[$i][0] ne "";$i++) {
				my @trigger = splitCommand($autoAI[$i][0], 2, 0);
				if ($trigger[0] eq "emsgnpcs" && existsInPatternList($trigger[1],$talk)) {
					insert_que($i);
				}elsif ($trigger[0] eq "msgnpcs" && existsInList($trigger[1],$talk)) {
					insert_que($i);
				}
			}
		}
	}
}

sub msg_console {
	my $args = $_[1];
	my $text = $args->{text};
	for (my $i = 0; $autoAI[$i][0] ne "";$i++) {
		my @trigger = splitCommand($autoAI[$i][0], 2, 0);
		if ($trigger[0] eq "consoleMsg" && existsInPatternList($trigger[1],$text)) {
			insert_que($i);
		}elsif ($trigger[0] eq "consoleMsg2" && $text =~ /$trigger[1]/ && $trigger[1] ne "") {
			insert_que($i);
		}
	}
}

sub selfCondition {
	my $condition = shift;
	my @cond =splitCommand($condition,2,0);
	my $monster;
	foreach (@monstersID) {
		next unless ($_);
		$monster = $actorList->get($_);
		next unless (ref($monster));
		last if($monster->{dmgFromYou2} > 0);
	}
	if ($cond[0] eq "hp_upper") {return 1 if ($char->{'percent_hp'} > $cond[1]);}
	if ($cond[0] eq "hp_lowwer") {return 1 if ($char->{'percent_hp'} < $cond[1]);}
	if ($cond[0] eq "sp_upper") {return 1 if ($char->{'percent_sp'} > $cond[1]);}
	if ($cond[0] eq "sp_lowwer") {return 1 if ($char->{'percent_sp'} < $cond[1]);}
	if ($cond[0] eq "minAggressives") {return 1 if (AIFunction::ai_getAggressives() < $cond[1]);}
	if ($cond[0] eq "maxAggressives") {return 1 if (AIFunction::ai_getAggressives() > $cond[1]);}
	if ($cond[0] eq "inStatus") {return 1 if (checkActorStatus($cond[1], $char));}
	if ($cond[0] eq "outStatus") {return 1 if (!checkActorStatus($cond[1], $char));}
	if ($cond[0] eq "whenAI") {return 1 if (!$AI->inQueue(split(/\s*,\s*/, $cond[1])));}
	if ($cond[0] eq "notWhenAI") {return 1 if ($AI->inQueue(split(/\s*,\s*/, $cond[1])));}
	if ($cond[0] eq "spirits_lowwer") {return 1 if ($char->{'spirits'} < $cond[1]);}
	if ($cond[0] eq "spirits_upper") {return 1 if ($char->{'spirits'} > $cond[1]);}
	if ($cond[0] eq "monsters") {return 1 if (existsInList2($cond[1], $monster->{name}, 2));}
	if ($cond[0] eq "monRace") {return 1 if (existsInList2($cond[1], $race_lut{$monster->{race}}, 2));}
	if ($cond[0] eq "monInStatus") {return 1 if (checkActorStatus($cond[1], $monster));}
	if ($cond[0] eq "monOutStatus") {return 1 if (!checkActorStatus($cond[1], $monster));}
	if ($cond[0] eq "totalDmg") {return 1 if ($monster->{dmgFromYou2} >= $cond[1]);}
	if ($cond[0] eq "weight") {return 1 if ($char->{percent_weight} == $cond[1]);}
	if ($cond[0] eq "weight_upper") {return 1 if ($char->{percent_weight} > $cond[1]);}
	if ($cond[0] eq "weight_lowwer") {return 1 if ($char->{percent_weight} < $cond[1]);}
	if ($cond[0] eq "inMap") {return 1 if (existsInList($cond[1],$field{name}) && $field{name} ne '');}
	if ($cond[0] eq "outMap") {return 1 if (!existsInList($cond[1],$field{name}) && $field{name} ne '');}
	return 0;
}

sub autoAI_step_command {
	my $commandr = shift;
	my @cmd = splitCommand($commandr,2,0);
	$command->parse($cmd[1]);
	shift @autoAI_que;
	$interface->writeoutput("autoAI: command :${cmd[1]}\n","yellow");
}

sub autoAI_step_move {
	my $commandr = shift;

	$autoAI_timeout{move_rep}{timeout} = 20 if(!defined($autoAI_timeout{move_rep}{timeout}));
	my @arg = splitCommand($commandr);
	my $map = $arg[3];
	$map =~ s/\s//g;
	$map = $field{name} if(!defined($map));

	if ($map eq $field{name} && distance($char->{pos_to}, {x => $arg[1], y => $arg[2]}) <= 2) {
		shift @autoAI_que;
		$AI->clear();
	}else{
		return if(!timeOut($autoAI_timeout{move_rep}));
		$AI->clear();
		$command->parse($commandr);
		$autoAI_timeout{move_rep}{timeout} = 20;
		$autoAI_timeout{move_rep}{time} = time;
		$interface->writeoutput("autoAI: Move to $arg[3] $arg[1] $arg[2]\n","yellow");
	}
}

sub autoAI_step_delay {
	my $commandr = shift;
	my @cmd = splitCommand($commandr);
	if (!$autoAI_temp{delay_wait}) {
		setupDelay();
		$autoAI_temp{delay_wait} = 1;
		$autoAI_timeout{delay}{time} = time;
		$autoAI_timeout{delay}{timeout} = getRand(split /\s*,\s*/, $cmd[1]);
		$interface->writeoutput("autoAI: Delay ${autoAI_timeout{delay}{timeout}}\n","yellow");
	}elsif(timeOut($autoAI_timeout{delay})){
		$command->parse("reload config");
		$command->parse("reload plus");
		$autoAI_temp{delay_wait} = 0;
		shift @autoAI_que;
	}
}

sub autoAI_step_delay2 {
	my $delay2_time = shift;
	if ($autoAI_temp{delay2_wait} != 1) {
		$autoAI_timeout{delay2}{time} = time;
		$autoAI_timeout{delay2}{timeout} = getRand(split /\s*,\s*/, $delay2_time);
		$interface->writeoutput("autoAI: Delay2 ${autoAI_timeout{delay2}{timeout}}\n","yellow");
		$autoAI_temp{delay2_wait} = 1;
	}elsif (timeOut($autoAI_timeout{delay2})) {
		shift @autoAI_que;
		$autoAI_temp{delay2_wait} = 0;
	}
}

sub autoAI_step_autoAI {
	my $commandr = shift;
	my $type = shift;
	my @cmd = splitCommand($commandr);
	if ($commandr eq "pause") {
		$autoAIState = 0;
		shift @autoAI_que if(!$type);
	}elsif ($commandr eq "stop" || $commandr eq "off") {
		$autoAIState = 0;
		undef @autoAI_que;
	}elsif ($commandr eq "clear") {
		undef @autoAI_que;
		undef %{$autoAI_temp{settime}};
		undef %{$autoAI_temp{varset}};
	}elsif ($commandr eq "queDel" || $commandr eq "queClear") {
		undef @autoAI_que;
	}elsif ($commandr eq "on") {
		$autoAIState = 1;
		shift @autoAI_que if(!$type);
	}elsif ($commandr eq "setTimeclear") {
		undef %{$autoAI_temp{settime}};
	}elsif ($cmd[0] eq "setTimeStart") {
		$autoAI_temp{settime}{$cmd[1]} = time;
	}elsif ($cmd[0] eq "setTimeDelete") {
		undef $autoAI_temp{settime}{$cmd[1]};
	}elsif ($cmd[0] eq "varSet") {
		if (defined($cmd[2])) {
			$autoAI_temp{varset}{$cmd[1]} = $cmd[2];
		}else{
			$autoAI_temp{varset}{$cmd[1]} = 1;
		}
		shift @autoAI_que if(!$type);
	}elsif ($cmd[0] eq "varDel") {
		$autoAI_temp{varset}{$cmd[1]} = 0;
		shift @autoAI_que if(!$type);
	}elsif ($cmd[0] eq "varClear") {
		undef %{$autoAI_temp{varset}};
		shift @autoAI_que if(!$type);
	}elsif ($cmd[0] eq "varAdd") {
		$autoAI_temp{varset}{$cmd[1]}++;
		shift @autoAI_que if(!$type);
	}elsif ($cmd[0] eq "varReduce") {
		$autoAI_temp{varset}{$cmd[1]}--;
		shift @autoAI_que if(!$type);
	}elsif ($commandr eq "reload") {
		if (defined($autoAI_temp{lastscript})) {
			changeScript($autoAI_temp{lastscript});
		}else{
			$command->parse("reload autoAI.tks");
		}
	}elsif ($cmd[0] eq "changeScript") {
		changeScript($cmd[1]);
	}else {
		my $que = scalar(@autoAI_que);
		$interface->writeoutput("autoAI: autoAI command syntax error\n","yellow");
		$interface->writeoutput("autoAI: syntax: autoAI <on|stop|pause|clear|setTimeStart|setTimeDeletesetTimeclear> (<bool>)\n","yellow");
		$interface->writeoutput("autoAI: status:".$autoAIState." inque:$que stepnow:${autoAI_que[0]}\n","yellow");
	}
	$interface->writeoutput("autoAI: autoAI $commandr .\n","yellow");
}

sub autoAI_step_talkNPC {
	my $commandr = shift;
	my @cmd = splitCommand($commandr, 4, 0);
	if ($cmd[3] eq "") {
		shift @autoAI_que;
		$autoAI_temp{talknpc}{talked} = 0;
		return;
	}
	my @step = splitCommand($cmd[3]);
	if ($step[0] eq "s") {
		$autoAI_temp{talknpc}{pos}{x} = $cmd[1];
		$autoAI_temp{talknpc}{pos}{y} = $cmd[2];
		$autoAI_temp{talknpc}{ID} = searchNPC(\%{$autoAI_temp{talknpc}{pos}});
		if ($autoAI_temp{talknpc}{ID} ne "") {
			changeDirection(\%{$autoAI_temp{talknpc}{pos}});
			$msgSender->sendTalk(pack("L", $autoAI_temp{talknpc}{ID}));
			$autoAI_timeout{talk_done} = time;
			my ($i,$newstep);
			for ($i = 1; $i < scalar(@step); $i++) {
				$newstep .= " $step[$i]";
			}
			$autoAI_que[0] = "$cmd[0] $cmd[1] $cmd[2]$newstep";
			$autoAI_temp{talknpc}{talked} = 1;
		}else {
			shift @autoAI_que;
			$autoAI_temp{talknpc}{talked} = 0;
			$interface->writeoutput("autoAI: ERROR - can\'t find talk npc.\n","yellow");
		}
	}
	if ($step[0] ne "s" && !$autoAI_temp{talknpc}{talked}) {
		shift @autoAI_que;
		$interface->writeoutput("autoAI: ERROR - talk command syntax error.\n","yellow");
	}

	if (timeOut($autoAI_timeout{talk_done},65) && defined($autoAI_timeout{talk_done})) {
		$msgSender->sendTalkCancel(pack("L", $autoAI_temp{talknpc}{ID}));
		$autoAI_temp{talknpc}{talked} = 0;
		shift @autoAI_que;
		undef $autoAI_timeout{talk_done};
		$interface->writeoutput("autoAI: npc talk : done : out of time.\n","yellow");
	}
}

sub autoAI_step_settime {
	my $timename = shift;
	$autoAI_temp{settime}{"$timename"} = time;
	shift @autoAI_que;
	$interface->writeoutput("autoAI: settime: $timename started.\n","yellow");
}

sub autoAI_step_settimedel {
	my $timename = shift;
	undef $autoAI_temp{settime}{"$timename"};
	shift @autoAI_que;
	$interface->writeoutput("autoAI: settime: $timename deleted.\n","yellow");
}

sub autoAI_step_direction {
	$autoAI_temp{change_direction}{x} = shift;
	$autoAI_temp{change_direction}{y} = shift;
	if (defined($autoAI_temp{change_direction}{x}) && defined($autoAI_temp{change_direction}{y})) {
		changeDirection($autoAI_temp{change_direction});
		$interface->writeoutput("autoAI: Redirection to ${autoAI_temp{change_direction}{x}} ${autoAI_temp{change_direction}{y}}.\n","yellow");
	}
	shift @autoAI_que;
}

sub setupDelay {
	$config{attackAuto} = 0;
	$config{avoidGM} = 0;
	$config{lockMap_0} = '';
	$config{lockMap_0_x} = '';
	$config{lockMap_0_y} = '';
	$config{teleportAuto_deadly} = 0;
	$config{teleportAuto_hp} = 0;
	$config{teleportAuto_idle} = 0;
	$config{teleportAuto_AtkMiss} = 0;
	$config{teleportAuto_maxDmg} = 0;
	$config{teleportAuto_maxDmgInLock} = 0;
	$config{teleportAuto_totalDmg} = 0;
	$config{teleportAuto_totalDmgLock} = 0;
	$config{teleportAuto_minAggressives} = 0;
	$config{teleportAuto_minAggressivesInLock} = 0;
	$config{teleportAuto_onlyWhenSafe} = 0;
	$config{teleportAuto_search} = 0;
	$config{teleportAuto_dist} = 0;
	$config{teleportAuto_attackedWhenSitting} = 0;
	$config{route_randomWalk} = 0;
	$config{itemsGatherAuto} = 0;
	$config{deadRespawn} = 0;
	$config{preferRoute} = 0;

	$AI->clear(qw[move route route_getMapRoute avoid]);
}

sub insert_que {
	my $i = shift;
	for (my $j = 1; $autoAI[$i][$j] ne ""; $j++) {
		my @cmd = splitCommand($autoAI[$i][$j],2,0);
		if ($cmd[0] eq "random") {
			my @cmdlist = split /\s*,\s*/, $cmd[1];
			my $rand = int rand(scalar(@cmdlist));
			push @autoAI_que, $cmdlist[$rand];
		}elsif ($cmd[0] eq "forceCommand") {
			autoAI_step_command($autoAI[$i][$j]);
		}elsif ($cmd[0] eq "varSet") {
			my @cmd2 = splitCommand($autoAI[$i][$j],3,0);
			if (defined($cmd2[2])) {
				$autoAI_temp{varset}{$cmd2[1]} = $cmd2[2];
			}else{
				$autoAI_temp{varset}{$cmd2[1]} = 1;
			}
		}elsif ($cmd[0] eq "varDel") {
			$autoAI_temp{varset}{$cmd[1]} = 0;
		}elsif ($cmd[0] eq "varClear") {
			undef %{$autoAI_temp{varset}};
		}elsif ($cmd[0] eq "varAdd") {
			$autoAI_temp{varset}{$cmd[1]}++;
		}elsif ($cmd[0] eq "varReduce") {
			$autoAI_temp{varset}{$cmd[1]}--;
		}else {
			push @autoAI_que, $autoAI[$i][$j];
		}
	}
}

sub parseScript {
    my ($file, $r_array) = @_;
    my ($line, $inblock);

	undef @{$r_array};

    return unless (open(F, "< $file"));

    foreach $line (<F>) {
        chomp($line);
        $line =~ s/^[\s\t]*(.*?)[\s\t]*$/$1/;
        next if (length($line) <= 0 || $line =~ /^\#/);

        if ($line =~ /^autoAI[\s\t]+(.+?)[\s\t]+\{$/) {
            if ($inblock) {
                croak(sprintf("Previous block are not closed before start new block\nLine: %d", $inblock));
            } else {
                push @{$r_array}, [$1];
                $inblock = $.;
            }
        } elsif ($line =~ /^\}$/) {
            $inblock = 0;
        } elsif ($inblock) {
            push @{$r_array->[-1]}, $line;
        } else {
            croak(sprintf("Line %d are not in some block"), $.);
        }
    }

    close F;
}

sub checkScriptTime {
	my @localtime = localtime shift;
	return if($config{"autoAI_0_start"} eq '' || $config{"autoAI_1_start"} eq '');
	for (my $i=0; $config{"autoAI_${i}_start"} ne ''; $i++) {
		my ($hr1,$min1,$sec1) = $config{"autoAI_${i}_start"}=~ /(\d+)\s*:\s*(\d+)\s*:\s*(\d+)/;
		my ($hr2,$min2,$sec2) = $config{"autoAI_${i}_end"}=~ /(\d+)\s*:\s*(\d+)\s*:\s*(\d+)/;
		my $timenow = $localtime[0] + $localtime[1] * 60 + $localtime[2] * 3600;
		my $starttime = $sec1 + $min1 * 60 + $hr1 * 3600;
		my $endtime = $sec2 + $min2 * 60 + $hr2 * 3600;
		my $loadfilename;
		if ($endtime > 0 && $timenow >= $starttime && $timenow <= $endtime && $autoAI_temp{lastscript} != $i) {
			changeScript($i);
			$autoAI_temp{lastscript} = $i;
		}elsif($timenow >= $starttime && $autoAI_temp{lastscript} != $i){
			changeScript($i);
			$autoAI_temp{lastscript} = $i;
		}
	}
}

sub changeScript {
	my $scriptNum = shift;
	my $loadfilename;
	if ($scriptNum == 0) {
		$loadfilename = "autoAI.tks";
	}else{
		$loadfilename = "autoAI${scriptNum}.tks";
	}
	$autoAI_temp{lastscript} = $scriptNum;
	parseScript(modifingPath('config', $loadfilename),\@autoAI);
	$interface->writeoutput("autoAI: change to script number ${scriptNum}.\n","yellow");
}

sub autoAISaveLog {
	my $text = shift;
	my $time_now = getFormattedDate(int time);
	open LOGS, ">> ".main::modifingPath('log', 'autoAILog.txt');
	print LOGS "[".$time_now."] : ".$text."\n";
	close LOGS;
}

sub serchChatRoomIDByPatternTitle {
	my $nameList = shift;
	foreach (@chatRoomsID) {
		if (existsInPatternList($nameList,$chatRooms{$_}{title})) {
			return $_;
		}
	}
	return;
}



return 1;