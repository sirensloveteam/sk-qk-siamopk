#################################################
#ThaiKoreEx :: Auto - Use Location Skill plugin. v.1.4
#This plugin is licensed under the GNU GPL
#Copyright 2007 by Valkyrie
#Credit UltimaWeapon
#mod : Hunter Heal  11-May-2009 To. 12-May-2009
#For ThaikoreEX2.5
#################################################
package LocationSkill;

use Time::HiRes qw(time);
use AI;
use Globals;
use Plugins;
use Utils;
use miscFunctions qw(checkCondition switchEquip getName);

our %ai_delay_time;

Plugins::register('LocationSkill', 'Auto - Use Location Skill', \&Unload);

Plugins::addHook(HOOK_AI_SKILLS, \&eventAI_skills);

sub Unload {
	Plugins::delHook(HOOK_AI_SKILLS, \&eventAI_skills);
	}

sub eventAI_skills {
	return if ($AI->action eq 'dead' || $AI->action eq 'route' || $config{"lockMap_$lockMapIndex"} ne $field{name});

	if ($AI->isIdle || $AI->is(qw[route route_getMapRoute follow attack])) {
		my $skillAlias;
		my $i = 0;
		undef $ai_v{useLocation_skill};
		undef $ai_v{useLocation_skill_lvl};

		while ($skill_con{"useLocation_skill_$i"}) {
			my $prefix = "useLocation_skill_$i";
			$skillAlias = $skills_rlut{lcCht($skill_con{$prefix})};			
				
				if (checkCondition(\%skill_con, $prefix, undef, $i) && 
				((exists $skillsSP_lut{$skillAlias} && $char->{sp} >= $skillsSP_lut{$skillAlias}{$skill_con{$prefix.'_lvl'}}) || 
				(%{$char->{skills}{$skillAlias}} && $char->{sp} >= $char->{skills}{$skillAlias}{sp}) || 
				(!exists $skillsSP_lut{$skillAlias} && !%{$char->{skills}{$skillAlias}}))) {
					$ai_v{$prefix.'_time'} = time;
					$ai_v{useLocation_skill} = $skill_con{$prefix};
					$ai_v{useLocation_skill_lvl} = $skill_con{$prefix.'_lvl'};
					$ai_v{useLocation_skill_maxCastTime} = $skill_con{$prefix.'_maxCastTime'};
					$ai_v{useLocation_skill_minCastTime} = $skill_con{$prefix.'_minCastTime'};
					$ai_v{useLocation_skill}{pos}{x} = $skill_con{$prefix.'_posX'};
					$ai_v{useLocation_skill}{pos}{y} = $skill_con{$prefix.'_posY'};
					$ai_v{useLocation_skill_delayTime} = $skill_con{$prefix.'_delayTime'};
					$ai_v{useLocation_skill_chk_pos_inSkill} = $skill_con{$prefix.'_chk_pos_inSkill'};
				last;
			}
			$i++;
		}

		if ($ai_v{useLocation_skill_lvl} > 0 && timeOut($ai_v{ai_delay_time},$ai_v{useLocation_skill_delayTime})) {									
					if ( $ai_v{useLocation_skill_chk_pos_inSkill} ne "" ) {
							my $typeno = chktype( $ai_v{useLocation_skill_chk_pos_inSkill} );
							my $chklocatskill = inskill ( $typeno,$ai_v{useLocation_skill}{pos}{x},$ai_v{useLocation_skill}{pos}{y}  );
						if ( $chklocatskill ) {
							$AI->ai_skillUse($char->{skills}{$skillAlias}{ID}, $ai_v{useLocation_skill_lvl}, $ai_v{useLocation_skill_maxCastTime}, $ai_v{useLocation_skill_minCastTime}, $ai_v{useLocation_skill}{pos}{x}, $ai_v{useLocation_skill}{pos}{y});											
							$ai_v{ai_delay_time}=time;
						}
					} else  {
						$AI->ai_skillUse($char->{skills}{$skillAlias}{ID}, $ai_v{useLocation_skill_lvl}, $ai_v{useLocation_skill_maxCastTime}, $ai_v{useLocation_skill_minCastTime}, $ai_v{useLocation_skill}{pos}{x}, $ai_v{useLocation_skill}{pos}{y});											
						$ai_v{ai_delay_time}=time;
					}			
		}
	}
}

###======== Utilities Functions ==========###
## Check Skill in pos X , Y
sub inskill {
		my ($typeno,$skillposx,$skillposy) = @_;
		foreach (keys %spells) {
			next unless ($_);
					if ( ( $spells{$_}{type} == $typeno ) && ( $spells{$_}{pos}{x} == $skillposx ) && ( $spells{$_}{pos}{y} == $skillposy ) ){
						return 0;
					}
	}	
return 1;
}

## Retun No. of type skill on 011F
sub chktype {
	my $skillcon = shift;
	foreach (keys %{$msgStrings{'011F'}}) {
		next unless ($_);
		my $skillname = $msgStrings{'011F'}{$_};
			if ( $skillname eq $skillcon ) {
				return $_;
			}		
	}
return 0;
}
return 1;