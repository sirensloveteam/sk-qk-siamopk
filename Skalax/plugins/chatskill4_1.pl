################################################
##ThaiKoreEx :: chatskill plugin
##This plugin is licensed under the GNU GPL
##Demo version 4.1 by Ninkoman
##For ThaiKoreEX 2.5  18/1/09
##Test on ThaikoreEX 2.5
################################################
package chatSkill;

use strict;
use Globals;
use Plugins;
use FileParser;
use Utils;
use Time::HiRes 'time';
use AIFunction;

our %chatSkill;
our @chatSkillTemp;
our %chatSkill_timeout;

Plugins::register('chatSkill', 'use buff to other player request', \&Unload);

Plugins::addHook(HOOK_START, \&start);
Plugins::addHook(HOOK_EVENT_CHAT, \&event_chat);
Plugins::addHook(HOOK_EVENT_PLAYER, \&event_player);
Plugins::addHook(HOOK_AI_PRE, \&ai_pre);
Plugins::addHook(HOOK_EVENT_YOU, \&event_you);

sub Unload {	
	Plugins::delHook(HOOK_START, \&start);
	Plugins::delHook(HOOK_EVENT_CHAT, \&event_chat);
	Plugins::delHook(HOOK_EVENT_PLAYER, \&event_player);
	Plugins::delHook(HOOK_AI_PRE, \&ai_pre);
	Plugins::addHook(HOOK_EVENT_YOU, \&event_you);
}

sub start {
	addParseFiles(\@parseFiles, modifingPath('config', "skillchat.txt"), \%chatSkill, \&parseDataFile2);
}

###Event###
sub event_chat {
	return if(!$config{chatSkill});
	my $args = $_[1];
	my $chatMsg = $args->{message};
	my $targetID = $args->{id};
	my $targetName = $args->{name};
	my $type = $args->{type};
	
	$targetID = findTargetID_byName($targetName) if (!defined($targetID) && defined($targetName));
	for (my $i = 0; $chatSkill{"chatSkill_$i"} ne ""; $i++) {
		my $chatSkill_match;
		if ($chatSkill{"chatSkill_${i}_type"}) {
			$chatSkill_match = existsInPatternList($chatSkill{"chatSkill_$i"}, $chatMsg);
		}else {
			$chatSkill_match = existsInList($chatSkill{"chatSkill_$i"}, $chatMsg);
		}
		if ($chatSkill_match) {
			my $existInTemp = 0;
			for (my $j = 0; defined($chatSkillTemp[$j]{targetID}); $j++) {
				if ($chatSkillTemp[$j]{targetID} eq $targetID && $chatSkillTemp[$j]{skillSlot} == $i) {
					$existInTemp = 1;
					last;
				}
			}
			next if(($chatSkill{"chatSkill_${i}_target"} ne ""
				&& $chatSkill{"chatSkill_${i}_target"} ne $targetName)
				|| !(findChatSkill_msgType($chatSkill{"chatSkill_${i}_msgType"},$type))
				|| !(findChatSkill_targetType($chatSkill{"chatSkill_${i}_targetType"}, $targetID))
				|| $existInTemp);
			
			my $chatSkillTemp_index = 0;
			for (my $i = 0; defined($chatSkillTemp[$i]{targetID}); $i++){
				$chatSkillTemp_index++;
			};
			$chatSkillTemp[$chatSkillTemp_index]{targetID} = $targetID;
			$chatSkillTemp[$chatSkillTemp_index]{skillSlot} = $i;
		}
	}
}

sub event_player {
	return if(!$config{chatSkill});
	my $args = $_[1];
	my $targetID = $args->{id};
	my $emotion = $args->{emotion};
	my $type = $args->{type};
	return if($type != EV_EMOTION || !$targetID);

	for (my $i = 0; $chatSkill{"chatSkill_${i}_emotion"} ne ""; $i++) {
		if (existsInList($chatSkill{"chatSkill_${i}_emotion"}, $emotion)) {
			my $player = $actorList->get($targetID);
			my $targetName = $player->{name};
			my $existInTemp = 0;
			for (my $j = 0; defined($chatSkillTemp[$j]{targetID}); $j++) {
				if ($chatSkillTemp[$j]{targetID} eq $targetID && $chatSkillTemp[$j]{skillSlot} == $i) {
					$existInTemp = 1;
					last;
				}
			}
			next if(($chatSkill{"chatSkill_${i}_target"} ne ""
			&& $chatSkill{"chatSkill_${i}_target"} ne $targetName)
			|| $existInTemp);

			my $conf_targetType = $chatSkill{"chatSkill_${i}_targetType"};
			my $checkTargetType = findChatSkill_targetType($conf_targetType,$targetID);
			next if(!$checkTargetType);

			my $chatSkillTemp_index = 0;
			for (my $i = 0; defined($chatSkillTemp[$i]{targetID}); $i++){
				$chatSkillTemp_index++;
			};
			$chatSkillTemp[$chatSkillTemp_index]{targetID} = $targetID;
			$chatSkillTemp[$chatSkillTemp_index]{skillSlot} = $i;
		}
	}
}

sub ai_pre {
	return if ($AI->inQueue(qw[skill_use]));

	$chatSkill_timeout{waitAfterUse}{timeout} = 0.5 if(!defined($chatSkill_timeout{waitAfterUse}{timeout}));
	return if(!$config{chatSkill} || !defined($chatSkillTemp[0]{targetID}) || !(timeOut($chatSkill_timeout{waitAfterUse})));
	my @chatSkillUse = shift(@chatSkillTemp);

	my $targetID = $chatSkillUse[0]{targetID};
	my $chatSkillSlot = $chatSkillUse[0]{skillSlot};

	$chatSkill_timeout{playerTimeout}{$targetID}{$chatSkillSlot}{timeout} = $chatSkill{"chatSkill_${chatSkillSlot}_playerTimeout"};
	return if(!timeOut($chatSkill_timeout{playerTimeout}{$targetID}{$chatSkillSlot}));
	if ($currentChatRoom ne '') {
			$msgSender->sendChatRoomLeave();
			sleep(0.5);
	}
	doChatSkill($targetID,$chatSkillSlot);
}

sub event_you {
	my $args = $_[1];
	return if($args->{type} != EV_ENTERING_GAME);
	$msgSender->sendGuildRequest(1);
}
####Function####
sub findTargetID_byName{
	my $targetName = shift;
	return if(!defined($targetName));

	foreach (@playersID) {
		next unless ($_);
		my $player = $actorList->get($_);
		my $playerName = $player->{name};
		if ($playerName eq $targetName) {
			return $_;
		}
	}
	return;
}

sub findChatSkill_msgType {
	my ($conf_msgType,$type) = @_;
	if ($conf_msgType & 1 && $type == EV_MESSAGE_CHAT) {
		return 1;

	}elsif ($conf_msgType & 2 && $type == EV_MESSAGE_PARTY) {
		return 1;

	}elsif ($conf_msgType & 4 && $type == EV_MESSAGE_GUILD) {
		return 1;

	}elsif ($conf_msgType & 8 && $type == EV_MESSAGE_PM) {
		return 1;

	}else {
		return 0;
	}
}

sub findChatSkill_targetType {
	my ($conf_targetType,$targetID) = @_;
	my ($partyMember,$guildMember);
	foreach (@partyUsersID) {
		next unless ($_);
		if ($_ eq $targetID) {
			$partyMember = 1;
		}
	}

	my $tartget = $actorList->get($targetID);
	if ($char->{guild}{ID} eq $tartget->{guildID}) {
		$guildMember = 1;
	}
	
	if ($conf_targetType & 1 && !$partyMember && !$guildMember) {
		return 1;

	}elsif ($conf_targetType & 2 && $partyMember){
		return 1;

	}elsif ($conf_targetType & 4 && $guildMember){
		return 1;

	}elsif ($conf_targetType == 0) {
		return 1;

	}else{
		return 0;
	}
}

sub search_tagetID_inscreen {
	my $targetID = shift;
	foreach (@playersID) {
		next unless ($_);
		if ($_ eq $targetID) {
			return 1;
		}
	}
	return 0;
}

###Use skill###
sub doChatSkill {
	my ($targetID,$chatSkillSlot) = @_;
	my $prefix = "chatSkill_${chatSkillSlot}";
	return if(!search_tagetID_inscreen($targetID) && $chatSkill{"${prefix}_targetType"} != 0);

	my $skillAlias = $skills_rlut{lcCht($chatSkill{"${prefix}_useSkill"})};
	my $player = $actorList->get($targetID);
	return if(!$char->{skills}{$skillAlias}{lv} || (distance(\%{$char->{'pos'}}, \%{$player->{'pos_to'}}) > 7 && $chatSkill{"${prefix}_targetType"} != 0));

	my $useskill_lv;
	if ($char->{skills}{$skillAlias}{lv} < $chatSkill{"${prefix}_lv"}) {
		$useskill_lv = $char->{skills}{$skillAlias}{lv};
	}else{
		$useskill_lv = $chatSkill{"${prefix}_lv"};
	}
	if ($chatSkill{"${prefix}_targetType"} == 0) {
		if (ai_getSkillUseType($skillAlias)) {
			$AI->ai_skillUse($char->{skills}{$skillAlias}{ID}, $useskill_lv, $chatSkill{"${prefix}_maxCastTime"}, $chatSkill{"${prefix}_minCastTime"}, $char->{pos_to}{x}, $char->{pos_to}{y});
		} else {
			$AI->ai_skillUse($char->{skills}{$skillAlias}{ID}, $useskill_lv, $chatSkill{"${prefix}_maxCastTime"}, $chatSkill{"${prefix}_minCastTime"}, $accountID);
		}
	}else{
		if (ai_getSkillUseType($skillAlias)) {
			$AI->ai_skillUse($char->{skills}{$skillAlias}{ID}, $useskill_lv, $chatSkill{"${prefix}_maxCastTime"}, $chatSkill{"${prefix}_minCastTime"}, $player->{pos_to}{x}, $player->{pos_to}{y});
		} else {
			$AI->ai_skillUse($char->{skills}{$skillAlias}{ID}, $useskill_lv, $chatSkill{"${prefix}_maxCastTime"}, $chatSkill{"${prefix}_minCastTime"}, $targetID);
		}
	}
	$chatSkill_timeout{waitAfterUse}{timeout} = $chatSkill{"chatSkill_${chatSkillSlot}_waitAfterUse"};
	$chatSkill_timeout{waitAfterUse}{time} = time;
	$chatSkill_timeout{playerTimeout}{$targetID}{$chatSkillSlot}{time} = time;
}

return 1;