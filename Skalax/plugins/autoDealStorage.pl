################################################
##ThaiKoreEx :: autoDealStorage plugin
##This plugin is licensed under the GNU GPL
##Version 1.0 by Ninkoman
##For ThaiKoreEX 2.5  22/03/09
##Test on ThaikoreEX 2.5 Hunter Heal Mod. 11/08
## mod deal zeny by sirenslove
################################################
package autoDealStorage;

use strict;
use Globals;
use Plugins;
use FileParser;
use Utils;

our %autoDealStorage_item;
our %autoDealStorage_temp;

Plugins::register('autoDealStorage', 'Deal important item befor storage for Thaikore EX 2.5', \&Unload);

Plugins::addHook(HOOK_START, \&start);
Plugins::addHook(HOOK_AI_PRE, \&ai_pre);
Plugins::addHook(HOOK_PACKET_POST, \&packet_post);
Plugins::addHook(HOOK_EVENT_YOU, \&event_you);
Plugins::addHook(HOOK_EVENT_YOU, \&entering_game);

sub Unload {	
	Plugins::delHook(HOOK_START, \&start);
	Plugins::delHook(HOOK_AI_PRE, \&ai_pre);
	Plugins::delHook(HOOK_PACKET_POST, \&packet_post);
	Plugins::delHook(HOOK_EVENT_YOU, \&event_you);
	Plugins::delHook(HOOK_EVENT_YOU, \&entering_game);
}

sub start {
	addParseFiles(\@parseFiles, modifingPath('config', "items_control.txt"), \%autoDealStorage_item, \&parseINI_lc);
}

sub ai_pre {
	return if((!$config{"lockMap_${lockMapIndex}_storage"} && !$autoDealStorage_temp{active}) || !$config{autoDealStorage});
	if (($AI->inQueue('storageAuto') || $char->{percent_weight} >= $config{itemsMaxWeight}) && !$autoDealStorage_temp{active2}) {
		$autoDealStorage_temp{iteminv} = 0;
		my $invIndex = serchDealItem();
		if ($invIndex ne '') {
			$autoDealStorage_temp{active} = 1;
			$autoDealStorage_temp{active2} = 1;
			$config{"lockMap_${lockMapIndex}_storage"} = 0;
			setManual();
			$AI->clear(qw[move route route_getMapRoute avoid]);
			$interface->writeoutput("autoDS: start.\n","yellow");
		}
	}
	if ($autoDealStorage_temp{active}) {
		my @dealpost = splitCommand($config{autoDealStorage_post},3,0);
		$autoDealStorage_temp{step} = 0 if(!defined($autoDealStorage_temp{step}));
		if ($autoDealStorage_temp{step} == 0 && timeOut($autoDealStorage_temp{time_step0},20)) {
			$command->parse("move ".$config{autoDealStorage_post});
			$autoDealStorage_temp{time_step0} = time;
		}elsif($autoDealStorage_temp{step} == 0 && $dealpost[2] eq $field{name} && (distance($char->{pos_to}, {x => $dealpost[0], y => $dealpost[1]}) < 3 || distance($char->{pos}, {x => $dealpost[0], y => $dealpost[1]}) < 3)){
			$autoDealStorage_temp{step} = 1;
			sleep(1);
			$autoDealStorage_temp{time_step1} = time;
		}elsif($autoDealStorage_temp{step} == 1 && timeOut($autoDealStorage_temp{time_step1},5)){
			my $playerID = serchTarget();
			if ($playerID) {
				sleep(1);
				$command->parse("deal ".$playerID);
				$autoDealStorage_temp{targetID_temp} = $playerID;
			}elsif(!$config{autoDealStorage_wait}){
				$autoDealStorage_temp{active} = 0;
				$autoDealStorage_temp{step} = 0;
				$command->parse("reload config");
				$command->parse("reload plus");
				$command->parse("autostorage");
			}
			$autoDealStorage_temp{time_step1} = time;
		}elsif($autoDealStorage_temp{step} == 2 && timeOut($autoDealStorage_temp{time_step2},2)){
			if ($autoDealStorage_temp{itemcount} < 9) {
				my $invIndex = serchDealItem();
				if ($invIndex eq '') {
				# mod by sirenslove - zeny trade 
				    my $zover = $config{autoDealStorage_zenyOver};
 					if ($char->{zenny} - $zover > 0) {
 						my $myzenytrade = $char->{zenny} - $zover;
						$command->parse("deal add z ".$myzenytrade);
					 }
				# end of mod by sirenslove
					$msgSender->sendDealFinalize();
					if ($autoDealStorage_temp{target_final}) {
						$msgSender->sendDealTrade();
						$autoDealStorage_temp{target_final} = 0;
					}
					$autoDealStorage_temp{step} = 3;
				}else{
					$command->parse("deal add ".$invIndex." ".$autoDealStorage_temp{deal_amount});
					$autoDealStorage_temp{iteminv} = $invIndex + $autoDealStorage_temp{inv_next};
					$autoDealStorage_temp{itemcount}++;
				}
			}else{
				# mod by sirenslove - zeny trade 
				    my $zover = $config{autoDealStorage_zenyOver};
 					if ($char->{zenny} - $zover > 0) {
 						my $myzenytrade = $char->{zenny} - $zover;
						$command->parse("deal add z ".$myzenytrade);
					 }
				# end of mod by sirenslove

				$msgSender->sendDealFinalize();
				if ($autoDealStorage_temp{target_final}) {
					sleep(1);
					$msgSender->sendDealTrade();
					$autoDealStorage_temp{target_final} = 0;
				}
				$autoDealStorage_temp{iteminv} = 0;
				$autoDealStorage_temp{step} = 1;
			}
			$autoDealStorage_temp{time_step2} = time;
		}
	}
}

sub packet_post {
	return if(!$config{autoDealStorage} || !$autoDealStorage_temp{active2} || $autoDealStorage_temp{step} < 1);
	my $args = $_[1];
	my $switch = $args->{switch};

	if ($switch eq "01F5") {
		my ($ID, $charLV) = unpack("x3 L S", $args->{msg});
		if ($currentDeal{name} eq $config{autoDealStorage_target}) {
			$autoDealStorage_temp{step} = 2;
			$autoDealStorage_temp{itemcount} = 0;
			$autoDealStorage_temp{iteminv} = 0;
			$autoDealStorage_temp{time_step2} = time;
		}else{
			$msgSender->sendCurrentDealCancel();
		}
	}elsif ($switch eq "00EA"){
		my ($index, $fail) = unpack("x2 S C", $args->{msg});
		if ($fail == 1) {
			$msgSender->sendDealFinalize();
			if ($autoDealStorage_temp{target_final}) {
				$msgSender->sendDealTrade();
				$autoDealStorage_temp{target_final} = 0;
			}
			$autoDealStorage_temp{iteminv} = 0;
			$autoDealStorage_temp{step} = 1;
			$autoDealStorage_temp{time_step1} = time;
		}
	}elsif($switch eq "00EC"){
		my $type = unpack("x2 C", $args->{msg});
		if ($type == 1){
			if ($autoDealStorage_temp{step} != 2) {
				$msgSender->sendDealTrade();
			}else{
				$autoDealStorage_temp{target_final} = 1;
			}
		}
	}elsif($switch eq "00EE"){
		$autoDealStorage_temp{iteminv} = 0;
		$autoDealStorage_temp{step} = 1;
		$autoDealStorage_temp{time_step1} = time;
	}
}

sub event_you {
	return if(!$config{autoDealStorage});
	my $args = $_[1];
	
	if($args->{type} == EV_DEAL_COMPLETED && $autoDealStorage_temp{active2}){
		$autoDealStorage_temp{iteminv} = 0;
		my $invIndex = serchDealItem();
		if ($invIndex ne '') {
			$autoDealStorage_temp{iteminv} = 0;
			$autoDealStorage_temp{step} = 0;			
		}else{
			$autoDealStorage_temp{active} = 0;
			$autoDealStorage_temp{step} = 0;
			$command->parse("reload config");
			$command->parse("reload plus");
			$command->parse("autostorage");
		}
	}elsif($args->{type} == EV_STORAGE_CLOSED){
		$autoDealStorage_temp{active2} = 0;
		$interface->writeoutput("autoDS: end.\n","yellow");
		# mod by nunda if close storage and no item deal type command quit
		my $closeafterend = $config{autoDealStorage_closeAfterEnd};
		my $invIndex = serchDealItem();
		if ($invIndex eq '') {
			if ($closeafterend > 0) {
				$command->parse("quit");			
			}
		}
		# end of mod
				
	}
}

sub entering_game {
	if ($autoDealStorage_temp{active2}) {
		$autoDealStorage_temp{step} = 0;
	}
}

sub serchTarget {
	foreach (@playersID) {
		next unless ($_);
		my $player = $actorList->get($_);
		if ($player->{name} eq $config{autoDealStorage_target} && distance($char->{pos_to}, $player->{pos_to}) < 3) {
			return $player->{nameID};
		}
	}
	return 0;
}

sub serchDealItem {
	for (my $i = $autoDealStorage_temp{iteminv}; $i < scalar(@{$char->{inventory}}); $i++) {
		next if (!%{$char->{inventory}[$i]} || $char->{inventory}[$i]{equipped} || ($char->{inventory}[$i]{broken} && $char->{inventory}[$i]{nameID} >= 9001 && $char->{inventory}[$i]{nameID} <= 9027));
		my $itemname = lcCht($char->{inventory}[$i]{name});
		if ($autoDealStorage_item{$itemname}{deal} > 0) {
			if ($char->{inventory}[$i]{amount} > $autoDealStorage_item{$itemname}{deal}) {
				$autoDealStorage_temp{deal_amount} = $autoDealStorage_item{$itemname}{deal};
				$autoDealStorage_temp{inv_next} = 1;
			}else{
				$autoDealStorage_temp{deal_amount} = $char->{inventory}[$i]{amount};
				$autoDealStorage_temp{inv_next} = 0;
			}
			return $i;
		}
	}
	return;
}

sub parseAutoDealStorage {
	my $file = shift;
	my $r_hash = shift;
	my ($key, @args, $argls);
	undef %{$r_hash};
	open FILE, $file;
	foreach (<FILE>) {
		next if (/^\s*\#/ || /^\s*\n/);
		s/\s*[\r\n]*$//;
		($key, $argls) = $_ =~ /(.+?)\s+(\d+.*)/;
		$key = lcCht(underToSpace($key));
		$key =~ s/^\s*(.*?)\s*$/$1/;
		@args = split /\s+/,$argls;
		if (defined($key)) {
			$r_hash->{$key}{deal} = $args[4];
		}
	}
	close FILE;
}

sub setManual {
	$config{attackAuto} = 0;
	$config{avoidGM} = 0;
	$config{lockMap_0} = '';
	$config{lockMap_0_x} = '';
	$config{lockMap_0_y} = '';
	$config{teleportAuto_deadly} = 0;
	$config{teleportAuto_hp} = 0;
	$config{teleportAuto_idle} = 0;
	$config{teleportAuto_AtkMiss} = 0;
	$config{teleportAuto_maxDmg} = 0;
	$config{teleportAuto_maxDmgInLock} = 0;
	$config{teleportAuto_totalDmg} = 0;
	$config{teleportAuto_totalDmgLock} = 0;
	$config{teleportAuto_minAggressives} = 0;
	$config{teleportAuto_minAggressivesInLock} = 0;
	$config{teleportAuto_onlyWhenSafe} = 0;
	$config{teleportAuto_search} = 0;
	$config{teleportAuto_dist} = 0;
	$config{teleportAuto_attackedWhenSitting} = 0;
	$config{route_randomWalk} = 0;
	$config{itemsTakeAuto} = 0;
	$config{itemsGatherAuto} = 0;
	$config{deadRespawn} = 0;
	$config{preferRoute} = 0;

}

return 1;




