#####################################################
##ThaiKoreEx :: chatwarp plugin
##This plugin is licensed under the GNU GPL
##Demo version 1 by Ninkoman
##For ThaiKoreSVN & EX 2.5  13/2/08
##Test on ThaikoreSVN183 & 306 *unsure to use on new version
######################################################
package chatWarp;

use strict;
use Globals;
use Plugins;
use FileParser;
use Utils;
use Time::HiRes 'time';

our %chatWarp;
our @chatWarpTemp;
our %chatWarp_timeout;
our $chatWarp_wait;
our $chatWarp_slot;

Plugins::register('chatWarp', 'open warp when other player request', \&Unload);

Plugins::addHook(HOOK_START, \&start);
Plugins::addHook(HOOK_EVENT_CHAT, \&event_chat);
Plugins::addHook(HOOK_AI_PRE, \&ai_pre);

sub Unload {	
	Plugins::delHook(HOOK_START, \&start);
	Plugins::delHook(HOOK_EVENT_CHAT, \&event_chat);
	Plugins::delHook(HOOK_AI_PRE, \&ai_pre);
}

sub start {
	addParseFiles(\@parseFiles, modifingPath('config', "warpchat.txt"), \%chatWarp, \&parseDataFile2);
}

sub event_chat {
	return if(!$config{chatWarp});
	my $args = $_[1];
	my $chatMsg = $args->{message};
	my $targetID = $args->{id};
	my $targetName = $args->{name};
	my $type = $args->{type};
	return if($type != EV_MESSAGE_PM && $type != EV_MESSAGE_PM);
	
	$targetID = findTargetID_byName($targetName) if (!$targetID && defined($targetName));
	return if(!$targetID);
	
	for (my $i = 0; $chatWarp{"chatWarp_$i"} ne ""; $i++) {
		my $confset = $chatWarp{"chatWarp_$i"};
		if (existsInPatternList($chatWarp{"chatWarp_$i"}, $chatMsg)) {
			$char->{skills}{AL_WARP}{range} = 5;
			my $chatWarpTemp_index = 0;
			for (my $i = 0; defined($chatWarpTemp[$i]{targetID}); $i++){
				$chatWarpTemp_index++;
			};
			$chatWarpTemp[$chatWarpTemp_index]{targetID} = $targetID;
			$chatWarpTemp[$chatWarpTemp_index]{warpSlot} = $i;
		}
	}
}
 
sub ai_pre {
	return if (!$config{chatWarp});
	if ($chatWarp_wait && defined($warp{responses}[0])) {
		selectWarp_dest($chatWarp_slot);
		$chatWarp_timeout{waitAfterUse}{time} = time;
		undef $chatWarp_wait;
		undef $chatWarp_slot;
		delete @warp{responses};
	}elsif (!$chatWarp_wait) {
		return if(!$AI->isIdle());
		$chatWarp_timeout{waitAfterUse}{timeout} = 5 if(!defined($chatWarp_timeout{waitAfterUse}{timeout}));
		return if(!defined($chatWarpTemp[0]{targetID}) || !(timeOut($chatWarp_timeout{waitAfterUse})));
		my @chatWarpUse = shift(@chatWarpTemp);

		my $targetID = $chatWarpUse[0]{targetID};
		my $warpSlot = $chatWarpUse[0]{warpSlot};

		my $target_inscreen = search_tagetID_inscreen($targetID);
		return if(!$target_inscreen);

		$chatWarp_timeout{warpSlot_timeout}{$warpSlot}{timeout} = 20 if(!defined($chatWarp_timeout{warpSlot_timeout}{$warpSlot}{timeout}));
		return if(!(timeOut($chatWarp_timeout{warpSlot_timeout}{$warpSlot})));
		chatWarp_useSkill();
		$chatWarp_slot = $warpSlot;
		$chatWarp_wait = 1;
	}
}
##################
sub selectWarp_dest {
	my $warpSlot = shift;
	my $destmap = $chatWarp{"chatWarp_${warpSlot}_destMap"};
	$chatWarp_timeout{warpSlot_timeout}{$warpSlot}{time} = time;
	my $i = 0;
	while ($warp{responses}[$i] ne '') {
		if ($warp{responses}[$i] eq $destmap) {
			$command->parse("warp ${i}");
			return;
		}
		$i++;
	}
	$command->parse("warp cancel");
}

sub chatWarp_useSkill {
	my $warpSlot = shift;
	$command->parse("warp open");
}
##################
sub findTargetID_byName{
	my $targetName = shift;
	return if(!defined($targetName));

	foreach (@playersID) {
		next unless ($_);
		my $player = $actorList->get($_);
		my $playerName = $player->{name};
		if ($playerName eq $targetName) {
			return $_;
		}
	}
	return;
}

sub search_tagetID_inscreen {
	my $targetID = shift;
	foreach (@playersID) {
		next unless ($_);
		if ($_ eq $targetID) {
			return 1;
		}
	}
	return 0;
}

return 1;