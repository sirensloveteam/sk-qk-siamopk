#################################################
# ThaiKoreEx :: Conditions PLUS v. 1.1 plugin
# This plugin is licensed under the GNU GPL
# For ThaikoreEx2.5 Fix Update 04-Nov-2008
# mod by Hunter Heal 06-Nov-2008  - 08-Nov-2008
# add "inMsg" and "afterTime" to fuctions bot 
# useSelf_skill , attackSkillSlot and other.
#################################################
package conditonsplus;

use strict;
use Time::HiRes qw/time/;
use Plugins;
use Globals;
use Utils qw(timeOut);

our %msgtime;
our $guitext ;
our %search;
our %slotdelay;
our %msglive;

Plugins::register('conditonsplus', 'Add Conditions in functions bot ThaikoreEx2.5 ', \&Unload);

Plugins::addHook( HOOK_MSG_CONSOLE, \&msg_console);
Plugins::addHook( HOOK_EVENT_KORE, \&even_kore);

sub Unload {
	Plugins::delHook( HOOK_MSG_CONSOLE, \&msg_console );
	Plugins::delHook( HOOK_EVENT_KORE, \&even_kore );
}

###### in Control File ########
sub even_kore {
		my $args = $_[1];		
	return if( $args->{type} != EV_CHECK_CONDITION );
		my $prefix=$args->{prefix};
		my $inmsg = $args->{control}{$prefix."_inMsg"};		
			chomp($inmsg);
		my $aftertime = $args->{control}{$prefix."_afterTime"};
			chomp($aftertime);					
#################  CHECK MASSEAGE ################# 
	if ( $args->{control}{$prefix."_inMsg"} ) {	
## Save inMsg in control
		if ( !exists $search{$inmsg} ) {
			$search{$inmsg } = 0 ;
			$msglive{$inmsg} = 0 ;
			$interface->writeoutput("= Load MSG on = $prefix =  \n");
			if ( $args->{control}{$prefix."_afterTime"} ) {
				$interface->writeoutput("= Load afterTime on = $prefix =  \n");
					#inster MSG Live time = Aftertime
				$msglive{$inmsg} = $aftertime;
			}
		}
			$args->{return} = $search{$inmsg};
			clrmsgtime();
}
################# END. CHECK MASSEAGE ##############

################# CHECK AFTER TIEM ##################
	if ( $args->{control}{$prefix."_afterTime"} && $args->{return} ) {
		$args->{return} = 0; 
		if  ( !exists $slotdelay{$prefix} ) {
			$slotdelay{$prefix} = time;
				$interface->writeoutput("= Save Time on = $prefix =  \n");
		} elsif ( $slotdelay{$prefix} == 0 ) {
			$slotdelay{$prefix} = time;
		} 
		if ( timeOut( $slotdelay{$prefix},$aftertime ) && $slotdelay{$prefix}  ) {
			$interface->writeoutput("= Clear Time = $prefix =  \n");
				$args->{return} = 1;
			$slotdelay{$prefix} = 0 ;
		}
}
################# END. CHECK AFTER TIME ##################
# Return Conditons 0 / 1	
	return $_[1];
}
## END. HOOK_EVENT_KORE, \&even_kore

## Check message on console ( GUI / Console )
##==== HOOK_MSG_CONSOLE ====##
sub msg_console {
	my $args = $_[1];
	my @msgkey = keys (%search);
	return if ( !scalar(@msgkey) );
		$guitext = $args->{text};
	foreach (@msgkey) {
		next unless ($_);
		if ( $guitext =~ /$_/  && $_ ne "") {
			$search{$_} = 1;			
			$msgtime{$_} = time;
		}
	}
}
##==== END...HOOK_MSG_CONSOLE ====##

#### Utileties ####
#### Clear Masseage TIME ###
sub clrmsgtime {
	my @msgkey = keys (%search);
	foreach (@msgkey) {
		next unless ($_);
		if ( timeOut($msgtime{$_},( $msglive{$_}+1 ) ) && $search{$_} ) {
			$search{$_} = 0;			
		}
	}
}
return 1;
