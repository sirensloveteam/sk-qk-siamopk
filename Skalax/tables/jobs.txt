#---------------------- ThaiKoreEx Tables Package ----------------------
# Original by UltimaWeapon
# Joho Sleipnir (EP13.2.1 Renewal)
#-----------------------------------------------------------------------
0 Novice
1 Swordman
2 Magician
3 Archer
4 Acolyte
5 Merchant
6 Thief
7 Knight
8 Priest
9 Wizard
10 Blacksmith
11 Hunter
12 Assassin
13 Peco Knight
14 Crusader
15 Monk
16 Sage
17 Rogue
18 Alchemist
19 Bard
20 Dancer
21 Peco Crusader
22 Wedding Suit
23 Super Novice
24 Gunslinger
25 Ninja
26 Christmas
27 Summer
4001 High Novice
4002 High Swordsman
4003 High Magician
4004 High Archer
4005 High Acolyte
4006 High Merchant
4007 High Thief
4008 Lord Knight
4009 High Priest
4010 High Wizard
4011 Mastersmith
4012 Sniper
4013 Assassin Cross
4014 Peco Lord Knight
4015 Paladin
4016 Champion
4017 Scholar
4018 Stalker
4019 Biochemist
4020 Minstrel
4021 Gypsy
4022 Peco Paladin
4023 Baby Novice
4024 Baby Swordsman
4025 Baby Magician
4026 Baby Archer
4027 Baby Acolyte
4028 Baby Merchant
4029 Baby Thief
4030 Baby Knight
4031 Baby Priest
4032 Baby Wizard
4033 Baby Blacksmith
4034 Baby Hunter
4035 Baby Assassin
4036 Baby Peco Knight
4037 Baby Crusader
4038 Baby Monk
4039 Baby Sage
4040 Baby Rogue
4041 Baby Alchemist
4042 Baby Bard
4043 Baby Dancer
4044 Baby Peco Crusader
4045 Super Baby
4046 Taekwon
4047 Taekwon Master
4048 Flying Taekwon Master
4049 Soul Linker
4050 Munak
4051 Death Knight
4052 Dark Collector
4054 Rune Knight
4055 Warlock
4056 Ranger
4057 Archbishop
4058 Mechanic
4059 Guillotine Cross
4060 Rune Knight
4061 Warlock
4062 Ranger
4063 Archbishop
4064 Mechanic
4065 Guillotine Cross
4066 Royal Guard
4067 Sorcerer
4068 Minstrel
4069 Wanderer
4070 Sura
4071 Generic
4072 Dark Chaser
4073 Royal Guard
4074 Sorcerer
4075 Minstrel
4076 Wanderer
4077 Sura
4078 Generic
4079 Dark Chaser
4080 Dragon Rune Knight
4081 Dragon Rune Knight
4082 Gryphon Royal Guard
4083 Gryphon Royal Guard
4084 Wolf Ranger
4085 Wolf Ranger
4086 Mado Mechanic
4087 Mado Mechanic
4096 Baby Rune Knight
4097 Baby Warlock
4098 Baby Ranger
4099 Baby Archbishop
4100 Baby Mechanic
4101 Baby Guillotine Cross
4102 Baby Royal Guard
4103 Baby Sorcerer
4104 Baby Minstrel
4105 Baby Wanderer
4106 Baby Sura
4107 Baby Genetic
4108 Baby Dark Chaser
4109 Baby Rune
4110 Baby Guard
4111 Baby Ranger
4112 Baby Mechanic
4190 Ultimate Super Novice
4191 Baby Ultimate Super Novice
4211 Kagerou
4212 Oboro